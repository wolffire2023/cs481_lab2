import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:lab2/socicon_icons.dart';

//Lab 2, bottom navigation/ bottom sheet

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);


  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int index = 0;

  List<Widget> tabs = [
    Dogs(),
    Cats(),
    ProfilePage()
  ];
  @override
  Widget build(BuildContext context) {
    //implement bottom navigation
    return Scaffold(
      appBar: AppBar(title: Text('Lab 2'),
      ),
      //displays the element in tabs
      body: Center(child: tabs.elementAt(index)),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.blue,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.white.withOpacity(.50),
        selectedFontSize: 12,
        unselectedFontSize: 12,
        currentIndex: index, onTap: (value) => setState(() {index = value;}),
        items: [
          //create each tab
           BottomNavigationBarItem( 
              title: Text('Dogs'),
              icon: Icon(Socicon.dog)),
           BottomNavigationBarItem( title: Text('Cats'), icon: Icon(Icons.photo)),
           BottomNavigationBarItem( 
              title: Text('Profile'), 
              icon: Icon(Icons.person))
        ],
      )
    );
  }
}
class Cats extends StatelessWidget{
@override
  Widget build(BuildContext context) {
  return MaterialApp(
      debugShowCheckedModeBanner: false,
    home: Scaffold(
      backgroundColor: Colors.orange,
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Cats are Purrfect", style: TextStyle(
              color: Colors.indigo,
              fontSize: 30,

            ),

            )
          ],

        ),
        backgroundColor: Colors.orange,
        elevation: 0,

      ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    padding: EdgeInsets.all(1.0),
                    child: Image(
                      image: AssetImage('assets/cat5.jpg'),
                    ),

                  ),
                  Container(
                      padding: EdgeInsets.all(1),
                      child: Image(
                        image: AssetImage('assets/cat6.jpg'),
                      )),

                ],
              ),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                      padding: EdgeInsets.all(1),
                      child: Image(
                        image: AssetImage('assets/cat2.jpg'),
                      )),


                ],
              ),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                      padding: EdgeInsets.all(1),
                      child: Image(
                        image: AssetImage('assets/cat3.jpg'),
                      )),
                  Container(
                      padding: EdgeInsets.all(1),
                      child: Image(
                        image: AssetImage('assets/cat4.jpg'),
                      )),
                ],
              ),
            ),

          ],
        )),

    );


  }
}
class Dogs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          backgroundColor: Colors.amber[300],
          appBar: AppBar(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Dogs Love To Play", style: TextStyle(
                  color: Colors.blue,
                  fontSize: 30
                ),),
              ],
            ),
         //   centerTitle: true,
            backgroundColor: Colors.amber[300],
           // toolbarHeight: 20,
            shadowColor: Colors.white,
            elevation: 8,
          ),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      padding: EdgeInsets.all(1.0),
                      child: Image(
                        image: AssetImage('assets/dog1.jpg'),
                      ),
                    ),
                    Container(
                        padding: EdgeInsets.all(1.0),
                        child: Image(
                          image: AssetImage('assets/dog2.jpg'),
                        )),
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      padding: EdgeInsets.all(1.0),
                      child: Image(
                        image: AssetImage('assets/dog3.jpg'),
                      ),
                    ),
                    Container(
                        padding: EdgeInsets.all(1.0),
                        child: Image(
                          image: AssetImage('assets/dog4.jpg'),
                        )),
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      padding: EdgeInsets.all(1.0),
                      child: Image(
                        image: AssetImage('assets/dog5.jpg'),
                      ),
                    ),
                    Container(
                        padding: EdgeInsets.all(1.0),
                        child: Image(
                          image: AssetImage('assets/dog6.jpg'),
                        )),
                  ],
                ),
              ),
            ],
          )),
    );
  }
}

class ProfilePage extends StatefulWidget {
  ProfilePage({Key key}) : super(key: key);


  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  var selection = '';
  int imageindex = 0;
  List image = ['assets/dog1.jpg', 'assets/dog2.jpg', 'assets/dog3.jpg', 'assets/dog4.jpg', 'assets/dog5.jpg', 'assets/dog6.jpg','assets/cat2.jpg','assets/cat3.jpg','assets/cat4.jpg','assets/cat5.jpg','assets/cat6.jpg'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          mainAxisSize: MainAxisSize.max,
          children: [
            //profile picture
        CircleAvatar(backgroundImage: getImage(), radius: 100),
        Text('Profile', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
        RaisedButton(color: Colors.blue[300], textColor: Colors.white,
              child: Text('Edit'),
              onPressed: () {
                print('text');
                displayModalBottomSheet();
              }
        ),
        ],
        ),
        ),
    );
  }

  void displayModalBottomSheet() {
    showModalBottomSheet<void> (context: context, builder: (BuildContext context) {
      return Column(mainAxisSize: MainAxisSize.min,
      children: [
        ListTile(
        leading: Icon(Icons.account_circle), title: Text('Cycle picture'),
        onTap: () {
          setState(() {
            selection = 'Cycle picture';
            cycleImage();
          });
          Navigator.pop(context);
        }
        ),

      ],
      );
    }
    );
 }
void cycleImage() {
  if(imageindex < image.length - 1) {imageindex++;}
  else{imageindex = 0;}
}

AssetImage getImage() {
  return AssetImage(image.elementAt(imageindex));
}
}